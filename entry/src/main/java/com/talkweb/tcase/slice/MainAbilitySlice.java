package com.talkweb.tcase.slice;

import com.talkweb.tcase.ResourceTable;
import com.talkweb.ohosx.provider.BasePageSliderProvider;
import com.talkweb.ohosx.provider.PageViewHolder;
import com.talkweb.tcase.pojo.PageInfo;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainAbilitySlice extends AbilitySlice {

    private PageSlider mPageSlider;
    private CasePageSliderProvider mPageSliderProvider;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mPageSlider = (PageSlider) findComponentById(ResourceTable.Id_page_slider_component);

        mPageSliderProvider = new CasePageSliderProvider();
        mPageSliderProvider.setSourceData(createAndInitPageData());
        mPageSlider.setProvider(mPageSliderProvider);
    }

    private List<PageInfo> createAndInitPageData(){
        List<PageInfo> initData = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            PageInfo pageInfo = new PageInfo();
            char tempChar = (char) (new Random().nextInt(26) + 65);
            pageInfo.setContent("Case " + String.valueOf(tempChar));
            pageInfo.setColor(new RgbColor(
                    new Random().nextInt(255),
                    new Random().nextInt(255),
                    new Random().nextInt(255)));
            initData.add(pageInfo);
        }
        return initData;
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    private static class CasePageSliderProvider extends BasePageSliderProvider<PageInfo, CasePageViewHolder> {

        @Override
        protected CasePageViewHolder onCreatePageViewHolder(ComponentContainer componentContainer, int position) {
            LayoutScatter layoutScatter = LayoutScatter.getInstance(componentContainer.getContext());
            Component component = layoutScatter.parse(ResourceTable.Layout_component_page, componentContainer, false);
            return new CasePageViewHolder(component);
        }

        @Override
        protected void onBindPageViewHolder(CasePageViewHolder holder, int position) {
            PageInfo pageInfo = getItem(position);
            holder.setText(pageInfo.getContent());
            holder.setBackground(pageInfo.getColor());
        }
    }

    private static class CasePageViewHolder extends PageViewHolder {

        private Text mTextCom;

        public CasePageViewHolder(Component component) {
            super(component);
            mTextCom = (Text) component.findComponentById(ResourceTable.Id_text_component);
        }

        public void setText(String text) {
            mTextCom.setText(text);
        }

        public void setBackground(RgbColor color) {
            ShapeElement element = new ShapeElement();
            element.setShape(ShapeElement.RECTANGLE);
            element.setRgbColor(color);
            mTextCom.setBackground(element);
        }
    }

}
