package com.talkweb.tcase.pojo;

import ohos.agp.colors.RgbColor;

public class PageInfo {

    private String content;

    private RgbColor color;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public RgbColor getColor() {
        return color;
    }

    public void setColor(RgbColor color) {
        this.color = color;
    }
}
