# PageSliderCase-ohos

#### 介绍
一个基于鸿蒙（HarmonyOS）系统java UI组件PageSlider相关的封装。在应用开发中，使用PageSIilder封装组件库，可以提交开发效率，降低开发难度。


#### 安装教程

1. 在项目根目录的build.gradle添加中央仓库

   ```gradle
   repositories {
       //...
       mavenCentral()
   }
   ```

2. 在module（如：entry）的build.gradle引用

   ```Gradle
   dependencies {
       //...
       implementation 'io.gitee.binai:ohosx-pageslider:1.0.0'
   }
   ```

#### 使用说明

1. 在工程layout目录下的布局xml文件中创建PageSlider。

   ```xml
   <?xml version="1.0" encoding="utf-8"?>
   <DirectionalLayout
       xmlns:ohos="http://schemas.huawei.com/res/ohos"
       ohos:height="match_parent"
       ohos:width="match_parent"
       ohos:alignment="center"
       ohos:orientation="vertical">
   
       <PageSlider
           ohos:id="$+id:page_slider_component"
           ohos:height="match_parent"
           ohos:width="match_parent"
           ohos:orientation="vertical"
           ohos:page_cache_size="2"/>
   
   </DirectionalLayout>
   ```

2. 在工程layout目录下的创建Page页面布局文件。比如：component_page.xml

   ```xml
   <?xml version="1.0" encoding="utf-8"?>
   <DependentLayout
       xmlns:ohos="http://schemas.huawei.com/res/ohos"
       ohos:height="match_parent"
       ohos:width="match_parent">
   
       <Text
           ohos:id="$+id:text_component"
           ohos:height="match_parent"
           ohos:width="match_parent"
           ohos:text_size="32fp"
           ohos:text_font="sans-serif-medium"
           ohos:text_weight="800"
           ohos:text_color="#ffffff"
           ohos:text_alignment="center"/>
   
   </DependentLayout>
   ```

3. 继承PageViewHolder类，绑定Page页面布局组件。比如：CasePageViewHolder

   ```java
   private static class CasePageViewHolder extends PageViewHolder {
   
       private Text mTextCom;
   
       public CasePageViewHolder(Component component) {
           super(component);
           mTextCom = (Text) component.findComponentById(ResourceTable.Id_text_component);
       }
   
       public void setText(String text) {
           mTextCom.setText(text);
       }
   
       public void setBackground(RgbColor color) {
           ShapeElement element = new ShapeElement();
           element.setShape(ShapeElement.RECTANGLE);
           element.setRgbColor(color);
           mTextCom.setBackground(element);
       }
   }
   ```

4. 继承BasePageSliderProvider类，绑定Page页面数据源和PageViewHolder。**需要重写BasePageSliderProvider类onCreatePageViewHolder和onBindPageViewHolder两个方法。**

   ***提示：***BasePageSliderProvider类继承于PageSliderProvider，并重写和封装了一些常用操作方法。包括提供了数据源的封装。

   ```java
   private static class CasePageSliderProvider extends BasePageSliderProvider<PageInfo, CasePageViewHolder> {
   
       @Override
       protected CasePageViewHolder onCreatePageViewHolder(ComponentContainer componentContainer, int position) {
           LayoutScatter layoutScatter = LayoutScatter.getInstance(componentContainer.getContext());
           Component component = layoutScatter.parse(ResourceTable.Layout_component_page, componentContainer, false);
           return new CasePageViewHolder(component);
       }
   
       @Override
       protected void onBindPageViewHolder(CasePageViewHolder holder, int position) {
           PageInfo pageInfo = getItem(position);
           holder.setText(pageInfo.getContent());
           holder.setBackground(pageInfo.getColor());
       }
   }
   ```

5. 初始Page页面元数据和PageSlider组件，并将PageSliderProvider对象绑定到PageSlider组件中。

   ```java
   public class MainAbilitySlice extends AbilitySlice {
   
       private PageSlider mPageSlider;
       private CasePageSliderProvider mPageSliderProvider;
   
       @Override
       public void onStart(Intent intent) {
           super.onStart(intent);
           super.setUIContent(ResourceTable.Layout_ability_main);
           mPageSlider = (PageSlider) findComponentById(ResourceTable.Id_page_slider_component);
   
           mPageSliderProvider = new CasePageSliderProvider();
           mPageSliderProvider.setSourceData(createAndInitPageData());
           mPageSlider.setProvider(mPageSliderProvider);
       }
   
       //初始化Page页面元数据
       private List<PageInfo> createAndInitPageData(){
           List<PageInfo> initData = new ArrayList<>();
           for (int i = 0; i < 15; i++) {
               PageInfo pageInfo = new PageInfo();
               char tempChar = (char) (new Random().nextInt(26) + 65);
               pageInfo.setContent("Case " + String.valueOf(tempChar));
               pageInfo.setColor(new RgbColor(
                       new Random().nextInt(255),
                       new Random().nextInt(255),
                       new Random().nextInt(255)));
               initData.add(pageInfo);
           }
           return initData;
       }
       ········
   }
   ```

6. 更多使用方法，可以参考源代码工程entry。


#### License

```text
Copyright 2021 aibin

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
