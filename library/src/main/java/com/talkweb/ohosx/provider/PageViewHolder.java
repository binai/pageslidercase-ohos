package com.talkweb.ohosx.provider;

import ohos.agp.components.Component;

public abstract class PageViewHolder {

    protected final Component component;

    public PageViewHolder(Component component) {
        this.component = component;
    }

    public final Component getComponent() {
        return component;
    }
}
