package com.talkweb.ohosx.provider;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class BasePageSliderProvider<M, VH extends PageViewHolder> extends PageSliderProvider {

    //页面加载数据源
    private final List<M> mPageSliderSourceData;

    //页面component组件缓存，主要保存当前处于活跃状态的component组件
    private final HashMap<Integer, VH> mPageSliderComponentCache;

    //页面component组件回收站
    private final Queue<VH> mPageSliderComponentRecyclers;

    public BasePageSliderProvider() {
        this.mPageSliderSourceData = new ArrayList<>();
        this.mPageSliderComponentCache = new HashMap<>();
        this.mPageSliderComponentRecyclers = new LinkedBlockingQueue<>();
    }

    public final void clearSourceData() {
        mPageSliderSourceData.clear();
    }

    public final void setSourceData(List<M> data) {
        if (!data.isEmpty()) {
            mPageSliderSourceData.clear();
            mPageSliderSourceData.addAll(data);
        }
    }

    public final void appendSourceData(List<M> data) {
        if (!data.isEmpty()) {
            mPageSliderSourceData.addAll(data);
        }
    }

    public final void removeSourceData(M data) {
        if (data != null) {
            mPageSliderSourceData.remove(data);
        }
    }

    public final VH getPageViewHolder(int position) {
        return mPageSliderComponentCache.get(position);
    }

    public final M getItem(int position) {
        return mPageSliderSourceData.get(position);
    }

    @Override
    public int getCount() {
        return mPageSliderSourceData.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int index) {
        if (componentContainer == null || index >= getCount()) {
            return Optional.empty();
        }

        //从视图回收站中获取组件，如果没有创建新的组件实例
        VH pageViewHolder = mPageSliderComponentRecyclers.poll();
        if (pageViewHolder == null) {
            pageViewHolder = onCreatePageViewHolder(componentContainer, index);
        }

        onBindPageViewHolder(pageViewHolder, index);
        componentContainer.addComponent(pageViewHolder.getComponent());
        mPageSliderComponentCache.put(index, pageViewHolder);

        return pageViewHolder;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object object) {
        if (componentContainer == null || index >= getCount()) {
            return;
        }

        if (object instanceof PageViewHolder) {
            componentContainer.removeComponent(((PageViewHolder) object).getComponent());
            mPageSliderComponentCache.remove(index);
            //回收已经被销毁的视图组件
            mPageSliderComponentRecyclers.offer((VH) object);
        }
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        if (object instanceof PageViewHolder) {
            return component == ((PageViewHolder) object).getComponent();
        }
        return component == object;
    }

    protected abstract VH onCreatePageViewHolder(ComponentContainer componentContainer, int position);

    protected abstract void onBindPageViewHolder(VH holder, int position);

}
